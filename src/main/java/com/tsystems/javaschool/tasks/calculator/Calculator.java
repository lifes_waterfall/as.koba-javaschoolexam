package com.tsystems.javaschool.tasks.calculator;

import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null || statement.isEmpty())
            return null;

        LinkedList<Character> stackOp = new LinkedList<>();
        LinkedList<Double> stackNum = new LinkedList<>();

        for (int i = 0; i < statement.length(); i++){
            char c = statement.charAt(i);

            if (Character.isDigit(c) ||
                    (c == '-' && (i == 0 || isParentheses(statement.charAt(i-1))))){ //for negative number
                StringBuilder str = new StringBuilder();
                boolean firstDot = true;

                str.append(statement.charAt(i));
                i++;

                while (i < statement.length() &&
                        (Character.isDigit(statement.charAt(i)) || statement.charAt(i)=='.')){

                    if (statement.charAt(i) == '.' && firstDot)
                        firstDot = false;
                    else if (statement.charAt(i) == '.' && !firstDot)
                        return null;

                    str.append(statement.charAt(i));
                    i++;
                }
                i--;

                if (str.toString().equals("-"))//wrong negative number
                    return null;

                stackNum.add(Double.parseDouble(str.toString()));
            } else if ((c == '(') && (i == 0 || isOperator(statement.charAt(i-1))))
                stackOp.add('(');
            else if (c == ')'){
                while (stackOp.getLast() != '('){
                    if (executeOperator(stackNum, stackOp.removeLast()) == -1)
                        return null;
                }
                stackOp.removeLast();
            } else if (isOperator(c)){
                while (!stackOp.isEmpty() && getPriority(stackOp.getLast()) >= getPriority(c)){
                    if (executeOperator(stackNum, stackOp.removeLast()) == -1)
                        return null;
                }
                stackOp.add(c);
            } else
                return null;
        }

        while (!stackOp.isEmpty()){
            if (executeOperator(stackNum, stackOp.removeLast()) == -1)
                return null;
        }

        Locale loc = Locale.US;
        NumberFormat nf = NumberFormat.getInstance(loc);
        nf.setMaximumFractionDigits(4);
        return nf.format(stackNum.get(0));
    }

    private boolean isOperator(char c){
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    private boolean isParentheses(char c){
        return c == '(' || c == ')';
    }

    private int executeOperator(LinkedList<Double> lst, char op){ //returns result code: 1 - ok; -1 - error
        if (!isOperator(op))
            return -1;

        if (lst.size() < 2)
            return -1;

        double second = lst.removeLast();
        double first = lst.removeLast();

        switch(op){
            case '+':
                lst.add(first+second);
                break;
            case '-':
                lst.add(first-second);
                break;
            case '*':
                lst.add(first*second);
                break;
            case '/':
                if (second != 0)
                    lst.add(first/second);
                else
                    return -1;
                break;
            default:
                return -1;
        }
        return 1;
    }

    private int getPriority(char c){
        switch (c){
            case '+':
                return 1;
            case '-':
                return 1;
            case '*':
                return 2;
            case '/':
                return 2;
            default:
                return -1;
        }
    }
}
