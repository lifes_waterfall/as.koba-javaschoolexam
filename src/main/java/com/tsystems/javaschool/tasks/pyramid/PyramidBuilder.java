package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers == null || inputNumbers.isEmpty() || inputNumbers.indexOf(null) != -1)
            throw new CannotBuildPyramidException();

        int count = inputNumbers.size();

        double l = (Math.sqrt(1+8*count)-1)/2; //height is calculated by: count = (1-l)/2*l

        if (l%1 != 0)
            throw new CannotBuildPyramidException();

        int h = (int)l;

        int w = 2*h - 1; //weight

        Collections.sort(inputNumbers);

        int[][] res = new int[h][w];

        int cur = 0;
        boolean needZero = false;

        for (int i = 0; i < h; i++){
            for (int j = 0; j < w; j++){
                if ((j >= h - i - 1) && (j <= h + i - 1) && !needZero){
                    res[i][j] = inputNumbers.get(cur);
                    cur++;
                    needZero = true;
                } else{
                    res[i][j] = 0;
                    needZero = false;
                }
            }
        }

        return res;
    }


}
