package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null){
            throw new IllegalArgumentException();
        }

        if (x.isEmpty())
            return true;

        if (y.isEmpty())
            return false;
        
        boolean found = true;

        Iterator<Object> iter2 = y.iterator();

        for (Object obj1 : x){
            while(iter2.hasNext()){
                Object obj2 = iter2.next();
                if (obj1.equals(obj2)){
                    found = true;
                    break;
                }
                else found = false;
            }
            if (!found)
                return false;
        }

        return found;
    }
}
